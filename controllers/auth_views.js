/** @format */

const express = require('express');
const app = express();
const { user_game } = require('../models');
const bcrypt = require('bcrypt');

module.exports = {
  registerPage: (req, res) => {
    res.render('register');
  },
  registerViews: async (req, res) => {
    const hashedPassword = await bcrypt.hash(req.body.kata_sandi, 10);
    user_game.create({
      nama_pengguna: req.body.nama_pengguna,
      email: req.body.email,
      kata_sandi: hashedPassword,
    })
      .then((user) => {
        res.redirect('/view/login');
      })
      .catch((err) => {
        res.redirect('/view/register');
      });
  },
};