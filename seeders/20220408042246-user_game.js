'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('user_game', [
      {
        nama_pengguna: "dikaUYE",
        email: "alifraihanzaa@gmail.com",
        kata_sandi: "dikdik",
        role_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
    
        nama_pengguna: "paul",
        kata_sandi: "paulll",
        role_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
       
        nama_pengguna: "JONS",
        kata_sandi: "hardian",
        role_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        
        nama_pengguna: "ilham",
        kata_sandi: "jenjen",
        role_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        
        nama_pengguna: "queen",
        kata_sandi: "barbie",
        role_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      }
    ], {});
    
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user_game', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
